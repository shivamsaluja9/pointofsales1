﻿using Newtonsoft.Json;
using Point_of_Sales.Models;
using Point_of_Sales.ServicesData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Point_of_Sales.WebApi
{
    public class ProductController : ApiController
    {
        ProductServices _ProductServices = null;
         List<ProductMDL> _ItemList = null;
        public ProductController(ProductServices productServices)
        {
            _ProductServices = productServices;
        }

        [HttpPost]
        
        [Route("api/Product/AddProductDetails")]
        public HttpResponseMessage AddProductDetails() {
            List<ProductMDL> obj = new List<ProductMDL>();
            var httpRequest = HttpContext.Current.Request;
            string ProductDetail = string.Empty;
            var formdata = httpRequest.Form;
            if (httpRequest.Form["ProductData"] != null)
            {
                ProductDetail = httpRequest.Form["ProductData"];
            }
            else
            {
                ProductDetail = "";
            }
            JavaScriptSerializer jss = new JavaScriptSerializer();
            obj = jss.Deserialize<List<ProductMDL>>(ProductDetail);
            HttpPostedFile ProductImage=null;
            if (httpRequest.Files != null && httpRequest.Files.Count > 0)
            {
                foreach (string img in httpRequest.Files)
                {
                    if (img == "ProductImage" && httpRequest.Files[img].ContentLength > 0)
                    {
                        ProductImage = httpRequest.Files[img];
                        string imagePath= System.Web.Hosting.HostingEnvironment.MapPath("~/Images");
                        if (!System.IO.Directory.Exists(imagePath))
                        {
                            System.IO.Directory.CreateDirectory(imagePath);                    //Create directory to save image
                        }
                        
                        ProductImage.SaveAs(imagePath + "\\" + ProductImage.FileName);

                    }

                }
            }
            obj[0].ProductImageName = ProductImage.FileName;

            MessageMDL msg = _ProductServices.AddProductDetails(obj[0]);
            return Request.CreateResponse(HttpStatusCode.OK, msg);
        }


        [HttpGet]
        [Route("api/Product/GetItemDetalis")]
        public IEnumerable<ProductMDL> GetItemDetalis(int PK_ProductId=0, string SearchBy="", string SearchValue="")
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["path"];
            List<ProductMDL> _List = new List<ProductMDL>();
            _List = _ProductServices.GetProductDetails(PK_ProductId, SearchBy, SearchValue);
            foreach (var item in _List)
            {
                item.ProductImageName= path+"//Images//"+item.ProductImageName;
            }
            return _List;
        }

        [HttpGet]
        [Route("api/Product/DeleteProduct")]
        public HttpResponseMessage DeleteProduct(int PK_ItemId )
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["path"];
            MessageMDL msg = new MessageMDL();
            msg = _ProductServices.DeleteProducts(PK_ItemId);
            return Request.CreateResponse(HttpStatusCode.OK, msg);

        }


    }
}
