﻿using Point_of_Sales.Models;
using Point_of_Sales.ServicesData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Point_of_Sales.WebApi
{
    public class SaleController : ApiController
    {
        public readonly SaleServices _SaleServices;
        public SaleController(SaleServices saleServices)
        {
            _SaleServices = saleServices;
        }

        [HttpPost]
        
        [Route("api/Sale/AddSaleDetails")]
        public HttpResponseMessage AddSaleDetails([FromBody]InvoiceMDL obj)
        {
            List<InvoiceMDL> _list = new List<InvoiceMDL>();
            List<SaleRequestMDL> _Request = new List<SaleRequestMDL>();
            JavaScriptSerializer jss = new JavaScriptSerializer();
            foreach (var item in obj.Detail)
            {
                _Request.Add(new SaleRequestMDL
                {
                    EmployeeId = obj.EmployeeId,
                    TotalAmount =obj.TotalAmount,
                    SubTotal =obj.SubTotal,
                    VAT =obj.VAT,
                    Quantity=item.Quantity,
                    ProductId=item.ProductId,
                    Discount = obj.Discount,
                });
            }


            _list= _SaleServices.AddSaleDetails(_Request);
            return Request.CreateResponse(HttpStatusCode.OK, _list);
        }
    }
}
