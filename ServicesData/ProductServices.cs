﻿using Point_of_Sales.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Point_of_Sales.ServicesData
{
    public class ProductServices:IProductServices
    {
        string _commandText = string.Empty;
        string Con = string.Empty;
        SqlCommand objSqlCommand;
        SqlDataAdapter objSqlDataAdapter;
        DataSet objdataSet;
        List<ProductMDL> _ItemList = null;
        public ProductServices()
        {
            Con = ConfigurationManager.ConnectionStrings["connectionString"].ToString();

        }

        public MessageMDL AddProductDetails(ProductMDL objproductMDL) {
            MessageMDL objMessage = new MessageMDL();
            try
            {

                List<SqlParameter> parms = new List<SqlParameter>()
            {
                new SqlParameter("@iPK_ProductId",objproductMDL.PK_ItemId),
                new SqlParameter("@cProductName",objproductMDL.ItemName),
                new SqlParameter("@cQuantity",objproductMDL.OpeningQuantity),
                new SqlParameter("@cDescription",objproductMDL.Description),
                new SqlParameter("@cImageName",objproductMDL.ProductImageName),
                new SqlParameter("@cCategoryName",objproductMDL.CategoryName),
                new SqlParameter("@cPrice",objproductMDL.ItemPrice),

                

            };
                objdataSet = new DataSet();
                objSqlCommand = new SqlConnection(Con).CreateCommand();
                objSqlCommand.CommandText = "AddEditProductDetails";
                objSqlCommand.CommandType = CommandType.StoredProcedure;


                objSqlCommand.Parameters.AddRange(parms.ToArray());
                objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objdataSet);

                if (objdataSet.Tables[0].Rows.Count > 0)
                {
                    objMessage.MessageId = objdataSet.Tables[0].Rows[0].Field<int>("Message_Id");
                    objMessage.Message = objdataSet.Tables[0].Rows[0].Field<string>("Message");

                }

            }
            catch (Exception ex)
            {

            }

            return objMessage;
        }

        public List<ProductMDL> GetProductDetails(int PK_ProductId,string SearchBy,string SearchValue)
        {
            List<ProductMDL> _List = new List<ProductMDL>();
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>()
            {
                new SqlParameter("@iPK_ProductId",PK_ProductId),
                new SqlParameter("@cSearchBy",SearchBy),
                new SqlParameter("@cSearchValue",SearchValue),

            };

                objdataSet = new DataSet();
                objSqlCommand = new SqlConnection(Con).CreateCommand();
                objSqlCommand.CommandText = "GetProductDetails";
                objSqlCommand.CommandType = CommandType.StoredProcedure;

                objSqlCommand.Parameters.AddRange(parms.ToArray());

                objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objdataSet);

                if (objdataSet.Tables[0].Rows[0].Field<int>("Message_Id") == 1)
                {
                    _List = objdataSet.Tables[1].AsEnumerable().Select(dr => new ProductMDL
                    {
                        PK_ItemId = dr.Field<int>("PK_ProductId"),
                        ItemName = dr.Field<string>("ProductName"),
                        OpeningQuantity = dr.Field<string>("Quantity"),
                        Description = dr.Field<string>("Descriptions"),
                        ProductImageName= dr.Field<string>("ImageName"),
                        CategoryName = dr.Field<string>("CategoryName"),
                        ItemPrice=dr.Field<string>("Price")
                    }).ToList();
                }

            }
            catch (Exception ex)
            {

            }
            return _List;
        }


        public MessageMDL DeleteProducts(int PK_ItemId)
        {
            MessageMDL objMessage = new MessageMDL();
            try
            {

                List<SqlParameter> parms = new List<SqlParameter>()
            {
                new SqlParameter("@iPK_ItemId",PK_ItemId),
            
            };
                objdataSet = new DataSet();
                objSqlCommand = new SqlConnection(Con).CreateCommand();
                objSqlCommand.CommandText = "DeleteProduct";
                objSqlCommand.CommandType = CommandType.StoredProcedure;


                objSqlCommand.Parameters.AddRange(parms.ToArray());
                objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objdataSet);

                if (objdataSet.Tables[0].Rows.Count > 0)
                {
                    objMessage.MessageId = objdataSet.Tables[0].Rows[0].Field<int>("Message_Id");
                    objMessage.Message = objdataSet.Tables[0].Rows[0].Field<string>("Message");

                }

            }
            catch (Exception ex)
            {

            }

            return objMessage;
        }

        

    }
}