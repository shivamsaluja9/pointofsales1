﻿using Point_of_Sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point_of_Sales.ServicesData
{
    interface ISaleServices
    {
        List<InvoiceMDL> AddSaleDetails(List<SaleRequestMDL> json);
    }
}
