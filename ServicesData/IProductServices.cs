﻿using Point_of_Sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point_of_Sales.ServicesData
{
    interface IProductServices
    {
        List<ProductMDL> GetProductDetails(int PK_ProductId,string SearchBy,string SearchValue);
        MessageMDL AddProductDetails(ProductMDL objproductMDL);
        MessageMDL DeleteProducts(int PK_ItemId);
    }
}
