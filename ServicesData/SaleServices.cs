﻿using Point_of_Sales.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Point_of_Sales.ServicesData
{
    public class SaleServices : ISaleServices
    {
        string _commandText = string.Empty;
        string Con = string.Empty;
        SqlCommand objSqlCommand;
        SqlDataAdapter objSqlDataAdapter;
        DataSet objdataSet;
        public SaleServices()
        {
            Con = ConfigurationManager.ConnectionStrings["connectionString"].ToString();

        }

        public List<InvoiceMDL> AddSaleDetails(List<SaleRequestMDL> json)
        {

            List<InvoiceMDL> _Datalist = new List<InvoiceMDL>();
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[7]
                            {
                            new DataColumn("EmployeeId"           , typeof(Int32)),
                            new DataColumn("TotalAmount"              , typeof(string)),
                            new DataColumn("SubTotal"             , typeof(string)),
                            new DataColumn("VAT"            , typeof(string)),
                            new DataColumn("Quantity"       , typeof(string)),
                            new DataColumn("ProductId"           , typeof(string)),
                            new DataColumn("Discount"           , typeof(string)),

                            });
                dt = ConvertTo(json);


                List<SqlParameter> parms = new List<SqlParameter>
               {
                new SqlParameter("@type_Sale",SqlDbType.Structured){ Value=dt},
              };
                
                objdataSet = new DataSet();
                objSqlCommand = new SqlConnection(Con).CreateCommand();
                objSqlCommand.CommandText = "SaveSaleDetails";
                objSqlCommand.CommandType = CommandType.StoredProcedure;


                objSqlCommand.Parameters.AddRange(parms.ToArray());
                objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(objdataSet);

                if (objdataSet.Tables[0].Rows.Count > 0)
                {
                    _Datalist = objdataSet.Tables[0].AsEnumerable().Select(dr => new InvoiceMDL
                    {
                        PK_InvoiceId = dr.Field<Int64>("PK_InvoiceNo"),
                        TotalAmount = dr.Field<string>("TotalAmount"),
                        SubTotal = dr.Field<string>("SubTotal"),
                        VAT = dr.Field<string>("VAT"),
                        Discount = dr.Field<string>("Discount"),
                        BillDate = dr.Field<string>("BillDate"),

                        Detail = objdataSet.Tables[1].AsEnumerable().Select(dr1 => new InvoiceDetails {
                            PK_Det_Invoice=dr1.Field<Int64>("PK_DET_Invoice"),
                            ProductId=dr1.Field<string>("ProductId"),
                            Quantity=dr1.Field<string>("ProductQty"),
                            ProductName=dr1.Field<string>("ProductName"),
                            ItemPrice=dr1.Field<string>("Price")
                        }).ToList()



                    }).ToList();

                }

            }
            catch (Exception ex)
            {

            }

            return _Datalist;
        }

        public static DataTable ConvertTo<T>(IList<T> list)
        {
            DataTable table = CreateTable<T>();
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (T item in list)
            {
                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                table.Rows.Add(row);
            }

            return table;
        }

        public static DataTable CreateTable<T>()
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            return table;
        }

    }
}