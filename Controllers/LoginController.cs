﻿using Point_of_Sales.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace Point_of_Sales.Controllers
{
    public class LoginController : ApiController
    {
      

        [Authorize]
        [HttpGet]
        [Route("Login/Get")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        [Route("Login/UserLogin")]
        public IHttpActionResult UserLogin([FromBody]LoginModel objVM)
        {
            var UserList = new List<LoginModel>();
            var user = new LoginModel();
            user.UserName = "shivi";
            user.Password = "123";
            UserList.Add(user);
            var objlist = UserList.FirstOrDefault(x => x.UserName == objVM.UserName && x.Password == objVM.Password);
            if (objlist==null) {
                string path = System.Configuration.ConfigurationManager.AppSettings["path"];
                string url = path +"/Home/Index";

                System.Uri uri = new System.Uri(url);

                return Redirect(uri);
            }
            else
            {
               string Message = createToken(objVM.UserName);
                string path = System.Configuration.ConfigurationManager.AppSettings["path"];
                string url = path  + "/Product/Index";

                System.Uri uri = new System.Uri(url);

                return Redirect(uri);
            }
            //if (objlist == null)
            //{
            //    var response = Request.CreateResponse(OK(), new LoginResponse
            //    {
            //        Status = "Invalid",
            //        Message = "Invalid User."
            //    });
            //    //return
            //}
            //else return new LoginResponse
            //{
            //    Status = "Success",
            //    Message = createToken(objVM.UserName)
            //};
           
        }

        private string createToken(string username)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddDays(7);

            var tokenHandler = new JwtSecurityTokenHandler();

            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, username)
            });

            const string sec = "ERMN05OPLoDvbTTa/QkqLNMI7cPLguaRyHzyg7n5qNBVjQmtBhz4SzYh4NBVCXi3KJHlSXKP+oi2+bXr6CUYTR==";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);


            //create the jwt
            var token = (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: "http://localhost:50191", audience: "http://localhost:50191",
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
    }
}
