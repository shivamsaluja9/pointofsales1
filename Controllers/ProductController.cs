﻿using Point_of_Sales.Models;
using Point_of_Sales.ServicesData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Point_of_Sales.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddEditProduct(int id = 0)
        {
            //ViewData["CategoryList"] = GetCategory();
            if (id != 0)
            {
                ProductServices obj = new ProductServices();
             List<ProductMDL>_list =  obj.GetProductDetails(id,"","");
               // _ItemList = GetItemDetalis(id, "", "");
                return View("AddEditProduct",_list[0]);
            }
            else
            {
                return View();

            }
        }
        
    }
}