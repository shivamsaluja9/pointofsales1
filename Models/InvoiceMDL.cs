﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point_of_Sales.Models
{
    public class InvoiceMDL
    {
        public Int64 PK_InvoiceId { get; set; }
        public string TotalAmount { get; set; }
        public string SubTotal { get; set; }
        public string VAT { get; set; }
        public string Discount { get; set; }
        public string InvoiceDate { get; set; }
        public int EmployeeId { get; set; }
        public string BillDate { get; set; }
        public List<InvoiceDetails> Detail { get; set; }
    }
    public class InvoiceDetails
    {
        public Int64 PK_Det_Invoice { get; set; }
        public string ProductId { get; set; }
        public string Quantity { get; set; }
        public string ProductName { get; set; }
        public string ItemPrice { get; set; }
    }
}