﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point_of_Sales.Models
{
    public class SaleRequestMDL
    {
        public int EmployeeId { get; set; }
        public string TotalAmount { get; set; }
        public string SubTotal { get; set; }
        public string VAT { get; set; }
      //  public string InvoiceDate { get; set; }
   
        public string Quantity { get; set; }
        public string ProductId { get; set; }
        public string Discount { get; set; }

    }
}