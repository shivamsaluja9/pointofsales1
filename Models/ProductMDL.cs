﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Point_of_Sales.Models
{
    public class ProductMDL
    {
        public int PK_ItemId { get; set; }
        [Required(ErrorMessage = "Please Enter Item Name")]
        public string ItemName { get; set; }
        [Required(ErrorMessage = "Please Enter Item Code")]
        public string ItemPrice { get; set; }
        public string ItemCategory { get; set; }
        [Required(ErrorMessage = "Please Select Item Category")]
        public string CategoryName { get; set; }
        public string PurchasePrice { get; set; }
        [Required(ErrorMessage = "Please Enter Quantity")]
        public string OpeningQuantity { get; set; }
        public string Description { get; set; }
        public string StockValue { get; set; }
        public HttpPostedFileBase ProductImage { get; set; }
        public string ProductImageName { get; set; }
    }
}