﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Point_of_Sales.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class LoginResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}